import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.net.*;

public class Servidor extends JFrame {

   // declaración de los objectos sockets
    ServerSocket server;
    Socket s;
    
    int clienteNum = 0;
    JTextArea display;

    public Servidor() {
        super("Servidor");
        display = new JTextArea(20, 5);
        add("Center", display);
        setSize(500, 300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void runServer() {
        String msj;
        try {
             // creación de la instancia socket para el cliente
            server=new ServerSocket(6000,100);
            display.setText("Esperando  cliente....\n\n");

            do {
                clienteNum++;
                s = server.accept();
                System.out.println("Atendiendo  cliente:  " + clienteNum);
                display.append("socket cliente : " + s.toString());
                // creación de hilo
                new ServidorHilo(display, server, s, clienteNum).start();
            } while (s != null);

        } catch (IOException ex) {     }
    }

    public static void main(String args[]) {
        Servidor s = new Servidor();
        s.runServer();
    }
}
