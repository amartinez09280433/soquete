import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;


 public class Cliente extends JFrame implements ActionListener{

    Container c;
    JTextField op1,op2,ipdir;
    JLabel l1,l2,l3,ip;
    JPanel pN,pC,pS;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
 
    public Cliente()
    {
        setTitle("Cliente");
        setSize(500,300);
        setLocation(200,200);
        setLayout(new GridLayout(4,1));
        c= getContentPane();
        c.setBackground(Color.cyan);
        // creación de las intancias y agregación a los paneles 
        // de las componentes GUI
        pN=new JPanel();
        ip=new JLabel("Dirección IP (111.111.111.111) " );
        ipdir=new JTextField(12);
        pN.add(ip);
        pN.add(ipdir);
        
        l1=new JLabel("Introduce valores de operandos");
        
        l2=new JLabel("Operando 1: ");
        op1= new JTextField(3);
        l3=new JLabel("Operando 2: ");
        op2= new JTextField(3);
        boton=new JButton("Calcular");
        
        pC=new JPanel();
        pC.add(l1);
        pC.add(l2);
        pC.add(op1);
        pC.add(l3);
        pC.add(op2);
        pC.add(boton);
        
        display =new JTextArea(20,20);
        
        pS= new JPanel();
        pS.add(display);
         
        c.add(pN);
        c.add(pC);
        c.add(pS);
        pack();
        this.setLocationRelativeTo(null);
        setVisible(true);
        setSize(600,300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Solicitando petición");
    	peticionServidor(s1,s2);
    }
    
    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
        Socket client;
        // declaración de los objetos para el flujo de datos
        DataInputStream input;
        DataOutputStream output;
        double suma;
        String Suma;                           
         try {
                // creación de la instancia del socket       
                //cambiar localhost por ip
                    String ipc=ipdir.getText();
                   client=new Socket(ipc,6000);
		    display.setText("Socket Creado....\n");
                // creación de las instancias para el flujo de datos
                    input= new DataInputStream(client.getInputStream());
                    output= new DataOutputStream(client.getOutputStream());
                    display.append("Enviando operando 1\n");
                  output.writeDouble(s1);
		     display.append("Enviando operando 2\n");
                  output.writeDouble(s2);
		     display.append ("Esperando respuesta del servidor....\n\n");
                  suma=input.readDouble();
                  Suma= String.valueOf (suma);
	           display.append("Resultado: "+ Suma+"\n\n");
                 display.append("Cerrando cliente\n\n");
                 client.close();
              }

                catch(IOException e){
                 e.printStackTrace();
                }
     }

       
    public static void main(String args[])
    {
        new Cliente();
        
    }
}

